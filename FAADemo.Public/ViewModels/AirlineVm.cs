﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAADemo.Public.ViewModels
{
    public class AirlineVm
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Name is require.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Airline code is required.")]
        public string AirlineCode { get; set; }
        [Required(ErrorMessage = "Phone number is required.")]
        public string PhoneNumber { get; set; }
    }
}
