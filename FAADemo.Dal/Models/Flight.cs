﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FAADemo.Dal.Models
{
    public class Flight
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string FlightNumber { get; set; }
        [Required]
        public Guid AirlineId { get; set; }
        [Required]
        public DateTime Departure { get; set; }
        [Required]
        public Guid DepartureLocationId { get; set; }
        [Required]
        public DateTime Arrival { get; set; }
        [Required]
        public Guid ArrivalLocationId { get; set; }
        [Required]
        public Guid AirplaneTypeId { get; set; }
        [Required]
        public double Price { get; set; }
        [DefaultValue(0)]
        public double Discount { get; set; }

        public virtual Airline Airline { get; set; }
        public virtual Airport DepartureAirport { get; set; }
        public virtual Airport ArrivalAirport { get; set; }
        public virtual AirplaneType AirplaneType { get; set; }
        public virtual ICollection<PassengerFlight> PassengerFlights { get; set; }
    }
}
