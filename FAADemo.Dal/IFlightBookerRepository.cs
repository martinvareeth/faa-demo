﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAADemo.Dal.Models;

namespace FAADemo.Dal
{
    public interface IFlightBookerRepository
    {
        Guid AddPassenger(Passenger passenger);
        List<Flight> GetAllFlights();
        List<Flight> GetSpecialFlights();
        Flight GetFlight(Guid flightId);
        List<Airport> GetAllAirports();
        Airport GetAirport(Guid airportId);
        List<Airline>  GetAllAirlines();
        Airline GetAirline(Guid airlineId);
        void AddAirport(Airport airport);
        Guid AddAirline(Airline airline);
    }
}
