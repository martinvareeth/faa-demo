using FAADemo.Dal.Models;

namespace FAADemo.Dal.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FAADemo.Dal.FlightBookerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(FAADemo.Dal.FlightBookerContext context)
        {

            context.Airlines.AddOrUpdate(a => a.Id,
                new Airline {AirlineCode = "AMA", Name = "American Airlines", PhoneNumber = "8173320092"},
                new Airline {AirlineCode = "DLT", Name = "Delta Airlines", PhoneNumber = "8123224377"},
                new Airline {AirlineCode = "UNT", Name = "United Airlines", PhoneNumber = "4256463373"},
                new Airline {AirlineCode = "EMR", Name = "United Emirates", PhoneNumber = "6123279000"},
                new Airline {AirlineCode = "ALA", Name = "Alaskan Airlines", PhoneNumber = "4267612525"},
                new Airline {AirlineCode = "FNT", Name = "Frontier Airlines", PhoneNumber = "5154283300"},
                new Airline {AirlineCode = "SPT", Name = "Spirit Airlines", PhoneNumber = "7623445667"}
                );



            context.Airports.AddOrUpdate(a => a.Id,
                new Airport
                {
                    AirportCode = "DFW",
                    Name = "Dallas Fort Worth International",
                    City = "Irving",
                    State = "TX",
                    Zipcode = "75261",
                    PhoneNumber = "9729733112"
                },
                new Airport
                {
                    AirportCode = "FAT",
                    Name = "Fresno Yosemite International",
                    City = "Fresno",
                    State = "CA",
                    Zipcode = "93727",
                    PhoneNumber = "5596214500"
                },
                new Airport
                {
                    AirportCode = "DEN",
                    Name = "Denver International",
                    City = "Denver",
                    State = "CO",
                    Zipcode = "80249",
                    PhoneNumber = "3033422000"
                },
                new Airport
                {
                    AirportCode = "FSM",
                    Name = "Fort Smith Regional",
                    City = "Fort Smith",
                    State = "AR",
                    Zipcode = "72903",
                    PhoneNumber = "4794527000"
                },
                new Airport
                {
                    AirportCode = "LAX",
                    Name = "Los Angeles International",
                    City = "Los Angeles",
                    State = "CA",
                    Zipcode = "90045",
                    PhoneNumber = "4246465252"
                },
                new Airport
                {
                    AirportCode = "SAN",
                    Name = "San Diego International",
                    City = "San Diego",
                    State = "CA",
                    Zipcode = "92101",
                    PhoneNumber = "6194002404"
                },
                new Airport
                {
                    AirportCode = "BOS",
                    Name = "Logan International",
                    City = "Boston",
                    State = "MA",
                    Zipcode = "02128",
                    PhoneNumber = "8002356426"
                },
                new Airport
                {
                    AirportCode = "SFO",
                    Name = "San Francisco International",
                    City = "San Francisco",
                    State = "CA",
                    Zipcode = "94128",
                    PhoneNumber = "6508218211"
                },
                new Airport
                {
                    AirportCode = "DAL",
                    Name = "Dallas Love Field",
                    City = "Dallas",
                    State = "TX",
                    Zipcode = "75235",
                    PhoneNumber = "2146706080"
                },
                new Airport
                {
                    AirportCode = "ORD",
                    Name = "O'Hare International",
                    City = "Chicago",
                    State = "IL",
                    Zipcode = "60666",
                    PhoneNumber = "8008326352"
                },
                new Airport
                {
                    AirportCode = "MKE",
                    Name = "General Mitchell International",
                    City = "Milwaukee",
                    State = "WI",
                    Zipcode = "53210",
                    PhoneNumber = "4145623490"
                },
                new Airport
                {
                    AirportCode = "SEA",
                    Name = "Seattle�Tacoma International",
                    City = "Seattle",
                    State = "WA",
                    Zipcode = "98158",
                    PhoneNumber = "2067875388"
                }
                );

            context.SaveChanges();

            context.AirplaneTypes.AddOrUpdate(a => a.Id,
                new AirplaneType
                {
                    Name = "Boeing 737-500",
                    FirstClassRows = 6,
                    FirstClassSeatsPerRow = 6,
                    Manufacturer = "Boeing",
                    ModelNumber = "737-500",
                    SeatsPerRow = 6,
                    TotalRows = 20
                },
                new AirplaneType
                {
                    Name = "Bombardier CRJ-100",
                    FirstClassRows = 0,
                    FirstClassSeatsPerRow = 0,
                    Manufacturer = "Bombardier",
                    ModelNumber = "CRJ-100",
                    SeatsPerRow = 4,
                    TotalRows = 14
                },
                new AirplaneType
                {
                    Name = "McDonnell Douglas MD-80",
                    FirstClassRows = 4,
                    FirstClassSeatsPerRow = 4,
                    Manufacturer = "McDonnell Douglas",
                    ModelNumber = "MD-80",
                    SeatsPerRow = 5,
                    TotalRows = 32
                },
                new AirplaneType
                {
                    Name = "McDonnell Douglas MD-90",
                    FirstClassRows = 4,
                    FirstClassSeatsPerRow = 5,
                    Manufacturer = "McDonnell Douglas",
                    ModelNumber = "MD-90",
                    SeatsPerRow = 5,
                    TotalRows = 34
                },
                new AirplaneType
                {
                    Name = "Boeing 747-400 ",
                    FirstClassRows = 9,
                    FirstClassSeatsPerRow = 4,
                    Manufacturer = "Boeing",
                    ModelNumber = "747-400",
                    SeatsPerRow = 10,
                    TotalRows = 56
                }
                );

            context.SaveChanges();

            context.Flights.AddOrUpdate(f => f.Id,
                new Flight
                {
                    AirlineId =
                        context.Airlines.Where(a => a.AirlineCode.Equals("AMA")).Select(a => a.Id).SingleOrDefault(),
                    AirplaneTypeId =
                        context.AirplaneTypes.Where(a => a.ModelNumber.Equals("MD-80"))
                            .Select(a => a.Id)
                            .SingleOrDefault(),
                    ArrivalLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("BOS")).Select(a => a.Id).SingleOrDefault(),
                    DepartureLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("DFW")).Select(a => a.Id).SingleOrDefault(),
                    Price = 267.08,
                    Discount = 15,
                    Arrival = Convert.ToDateTime("9/23/2015 7:00PM"),
                    Departure = Convert.ToDateTime("9/23/2015 11:00PM"),
                    FlightNumber = "AA2338"
                },
                new Flight
                {
                    AirlineId =
                        context.Airlines.Where(a => a.AirlineCode.Equals("AMA")).Select(a => a.Id).SingleOrDefault(),
                    AirplaneTypeId =
                        context.AirplaneTypes.Where(a => a.ModelNumber.Equals("MD-80"))
                            .Select(a => a.Id)
                            .SingleOrDefault(),
                    ArrivalLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("LAX")).Select(a => a.Id).SingleOrDefault(),
                    DepartureLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("DEN")).Select(a => a.Id).SingleOrDefault(),
                    Price = 288.71,
                    Discount = 10,
                    Arrival = Convert.ToDateTime("9/18/2015 7:00AM"),
                    Departure = Convert.ToDateTime("9/18/2015 8:00AM"),
                    FlightNumber = "AA2708"
                },
                new Flight
                {
                    AirlineId =
                        context.Airlines.Where(a => a.AirlineCode.Equals("UNT")).Select(a => a.Id).SingleOrDefault(),
                    AirplaneTypeId =
                        context.AirplaneTypes.Where(a => a.ModelNumber.Equals("737-500"))
                            .Select(a => a.Id)
                            .SingleOrDefault(),
                    ArrivalLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("FSM")).Select(a => a.Id).SingleOrDefault(),
                    DepartureLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("FAT")).Select(a => a.Id).SingleOrDefault(),
                    Price = 471.10,
                    Discount = 10,
                    Arrival = Convert.ToDateTime("9/27/2015 9:08AM"),
                    Departure = Convert.ToDateTime("9/27/2015 2:17PM"),
                    FlightNumber = "277"
                },
                new Flight
                {
                    AirlineId =
                        context.Airlines.Where(a => a.AirlineCode.Equals("UNT")).Select(a => a.Id).SingleOrDefault(),
                    AirplaneTypeId =
                        context.AirplaneTypes.Where(a => a.ModelNumber.Equals("CRJ-100"))
                            .Select(a => a.Id)
                            .SingleOrDefault(),
                    ArrivalLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("ORD")).Select(a => a.Id).SingleOrDefault(),
                    DepartureLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("MKE")).Select(a => a.Id).SingleOrDefault(),
                    Price = 108.10,
                    Discount = 10,
                    Arrival = Convert.ToDateTime("9/12/2015 7:00PM"),
                    Departure = Convert.ToDateTime("9/12/2015 8:00PM"),
                    FlightNumber = "470"
                },
                new Flight
                {
                    AirlineId =
                        context.Airlines.Where(a => a.AirlineCode.Equals("DLT")).Select(a => a.Id).SingleOrDefault(),
                    AirplaneTypeId =
                        context.AirplaneTypes.Where(a => a.ModelNumber.Equals("MD-90"))
                            .Select(a => a.Id)
                            .SingleOrDefault(),
                    ArrivalLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("DAL")).Select(a => a.Id).SingleOrDefault(),
                    DepartureLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("SFO")).Select(a => a.Id).SingleOrDefault(),
                    Price = 99.08,
                    Discount = 10,
                    Arrival = Convert.ToDateTime("9/17/2015 4:06PM"),
                    Departure = Convert.ToDateTime("9/17/2015 7:22PM"),
                    FlightNumber = "560"
                },
                new Flight
                {
                    AirlineId =
                        context.Airlines.Where(a => a.AirlineCode.Equals("AMA")).Select(a => a.Id).SingleOrDefault(),
                    AirplaneTypeId =
                        context.AirplaneTypes.Where(a => a.ModelNumber.Equals("MD-80"))
                            .Select(a => a.Id)
                            .SingleOrDefault(),
                    ArrivalLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("DFW")).Select(a => a.Id).SingleOrDefault(),
                    DepartureLocationId =
                        context.Airports.Where(a => a.AirportCode.Equals("SEA")).Select(a => a.Id).SingleOrDefault(),
                    Price = 188.98,
                    Discount = 15,
                    Arrival = Convert.ToDateTime("9/15/2015 6:30AM"),
                    Departure = Convert.ToDateTime("9/15/2015 3:07PM"),
                    FlightNumber = "AA2704"
                }
                );
        }
    }
}
