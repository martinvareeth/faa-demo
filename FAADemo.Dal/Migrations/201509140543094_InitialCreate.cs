namespace FAADemo.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Airlines",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        AirlineCode = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Flights",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FlightNumber = c.String(nullable: false),
                        AirlineId = c.Guid(nullable: false),
                        Departure = c.DateTime(nullable: false),
                        DepartureLocationId = c.Guid(nullable: false),
                        Arrival = c.DateTime(nullable: false),
                        ArrivalLocationId = c.Guid(nullable: false),
                        AirplaneTypeId = c.Guid(nullable: false),
                        Price = c.Double(nullable: false),
                        Discount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Airlines", t => t.AirlineId, cascadeDelete: true)
                .ForeignKey("dbo.AirplaneTypes", t => t.AirplaneTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Airports", t => t.ArrivalLocationId)
                .ForeignKey("dbo.Airports", t => t.DepartureLocationId)
                .Index(t => t.AirlineId)
                .Index(t => t.DepartureLocationId)
                .Index(t => t.ArrivalLocationId)
                .Index(t => t.AirplaneTypeId);
            
            CreateTable(
                "dbo.AirplaneTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Manufacturer = c.String(nullable: false),
                        ModelNumber = c.String(nullable: false),
                        TotalRows = c.Int(nullable: false),
                        SeatsPerRow = c.Int(nullable: false),
                        FirstClassRows = c.Int(nullable: false),
                        FirstClassSeatsPerRow = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Airports",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        AirportCode = c.String(nullable: false, maxLength: 3),
                        City = c.String(nullable: false),
                        State = c.String(nullable: false, maxLength: 2),
                        Zipcode = c.String(nullable: false, maxLength: 5),
                        PhoneNumber = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PassengerFlights",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        PassengerId = c.Guid(nullable: false),
                        FlightId = c.Guid(nullable: false),
                        RowNumber = c.Int(nullable: false),
                        SeatNumber = c.Int(nullable: false),
                        SeatPrice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Flights", t => t.FlightId, cascadeDelete: true)
                .ForeignKey("dbo.Passengers", t => t.PassengerId, cascadeDelete: true)
                .Index(t => t.PassengerId)
                .Index(t => t.FlightId);
            
            CreateTable(
                "dbo.Passengers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        Gender = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PassengerFlights", "PassengerId", "dbo.Passengers");
            DropForeignKey("dbo.PassengerFlights", "FlightId", "dbo.Flights");
            DropForeignKey("dbo.Flights", "DepartureLocationId", "dbo.Airports");
            DropForeignKey("dbo.Flights", "ArrivalLocationId", "dbo.Airports");
            DropForeignKey("dbo.Flights", "AirplaneTypeId", "dbo.AirplaneTypes");
            DropForeignKey("dbo.Flights", "AirlineId", "dbo.Airlines");
            DropIndex("dbo.PassengerFlights", new[] { "FlightId" });
            DropIndex("dbo.PassengerFlights", new[] { "PassengerId" });
            DropIndex("dbo.Flights", new[] { "AirplaneTypeId" });
            DropIndex("dbo.Flights", new[] { "ArrivalLocationId" });
            DropIndex("dbo.Flights", new[] { "DepartureLocationId" });
            DropIndex("dbo.Flights", new[] { "AirlineId" });
            DropTable("dbo.Passengers");
            DropTable("dbo.PassengerFlights");
            DropTable("dbo.Airports");
            DropTable("dbo.AirplaneTypes");
            DropTable("dbo.Flights");
            DropTable("dbo.Airlines");
        }
    }
}
