﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAADemo.Dal.Models;

namespace FAADemo.Dal
{
    public class FlightBookerRepository : IFlightBookerRepository
    {
        private readonly FlightBookerContext _context;

        public FlightBookerRepository(FlightBookerContext context)
        {
            _context = context;
        }

        public Guid AddPassenger(Passenger passenger)
        {
            _context.Passengers.Add(passenger);
            _context.SaveChanges();

            return passenger.Id;
        }

        public List<Flight> GetAllFlights()
        {
            return _context.Flights.ToList();
        }

        public List<Flight> GetSpecialFlights()
        {
            return _context.Flights.Where(f=>f.Discount > 0).Select(f => f).ToList();
        }

        public Flight GetFlight(Guid flightId)
        {
            return _context.Flights.Where(f => f.Id.Equals(flightId)).Select(f => f).SingleOrDefault();
        }

        public List<Airport> GetAllAirports()
        {
            return _context.Airports.ToList();
        }

        public Airport GetAirport(Guid airportId)
        {
            return _context.Airports.Where(a => a.Id.Equals(airportId)).Select(a => a).SingleOrDefault();
        }

        public List<Airline> GetAllAirlines()
        {
            return _context.Airlines.ToList();
        }

        public Airline GetAirline(Guid airlineId)
        {
            return _context.Airlines.Where(a => a.Id.Equals(airlineId)).Select(a => a).SingleOrDefault();
        }

        public void AddAirport(Airport airport)
        {
            _context.Airports.Add(airport);
            _context.SaveChanges();
        }

        public Guid AddAirline(Airline airline)
        {
            _context.Airlines.Add(airline);
            _context.SaveChanges();

            return airline.Id;

        }
    }
}
