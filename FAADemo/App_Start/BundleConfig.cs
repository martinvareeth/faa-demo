﻿using System.Web;
using System.Web.Optimization;

namespace FAADemo
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/Lib/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/Lib/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/flightBooker").Include(
                "~/Scripts/Lib/angular.js",
                "~/Scripts/Lib/angular-resource.js",
                "~/Scripts/Lib/angular-route.js",
                 "~/Scripts/Lib/kendo.all.min.js",
                 "~/Scripts/Lib/kendo.angular.min.js",
                "~/Scripts/App/app.js",
                "~/Scripts/Services/flightService.js",
                "~/Scripts/Services/airlineService.js",
                "~/Scripts/Services/airportService.js",
                "~/Scripts/Services/passengerService.js",
                "~/Scripts/Services/dashboardService.js",
                "~/Scripts/Controllers/flightController.js",
                "~/Scripts/Controllers/airlineController.js",
                "~/Scripts/Controllers/airportController.js",
                "~/Scripts/Controllers/passengerController.js",
                "~/Scripts/Controllers/dashboardController.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/Lib/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/Lib/bootstrap.js",
                      "~/Scripts/Lib/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
