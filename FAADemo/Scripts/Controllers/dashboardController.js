﻿"use strict";

(function () {
    var module = angular.module("flightBooker");

    var dashboardController = function ($scope, $routeParams, dashboardService) {
        $scope.specials = dashboardService.GetSpecialFlights();

        $scope.selectFlight = function(flight) {
            // write code to do something here when table row seleced
        }

    };

    module.controller("dashboardController", ["$scope", "$routeParams", "dashboardService", dashboardController]);

}());