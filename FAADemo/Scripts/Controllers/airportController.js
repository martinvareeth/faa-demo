﻿"use strict";

(function () {
    angular.module('flightBooker').controller('airportController', [
        '$scope', 'airportService', function ($scope, airportService) {
            
            $scope.airportGridOptions = {
                columns: [
                    { 'field': 'airportCode', 'title': 'Airport Code' },
                    { 'field': 'name', 'title': 'Name' }
                ],
                dataSource: {
                    transport: {
                        read: function(options) {
                            airportService.Common.GetAllAirports(function (data) {
                                options.success(data);
                            });
                        }

                    },
                    pageSize: 5
                },
                groupable: true,
                scrollable: false,
                sortable: true,
                pageable: true,
                selectable: 'row'
            };

            $scope.selectionChange = function(dataItem) {
                $scope.selectedItem = dataItem;
            };

            $scope.save = function(airport) {
               airportService.Common.SaveAirport(airport).$promise.then(function() {
                   $scope.errors = "";
                   $scope.airportSave = airport;
               }, function(response) {
                   $scope.errors = response.data;
               });
            }

        }
    ]);
})();