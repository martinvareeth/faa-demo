﻿"use strict";

(function () {
    var module = angular.module("flightBooker");

    var flightController = function($scope, $routeParams, flightService) {

        $scope.save = function(flight) {
            flightService.save(flight).$promise.then(
                function () { $location.url("/FlightBooker/Index") },
                function (response) { $scope.errors = response.data; }
            );
        };

    };

    module.controller("flightController", ["$scope", "$routeParams", "flightService", flightController]);

}());