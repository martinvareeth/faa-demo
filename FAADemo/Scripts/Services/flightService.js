﻿"use strict";

(function () {
    
    var flightService = function($resource) {
        return {
            save: function(flight) {
                return $resource("/api/flight").save(flight);
            }
        };
    };

    var module = angular.module("flightBooker");
    module.factory("flightService", ["$resource", flightService]);
}());