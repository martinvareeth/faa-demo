﻿"use strict";

(function () {

    var dashboardService = function ($resource) {
        return {
            GetSpecialFlights: function() {
                return $resource("api/Flight").query();
            }
        };
    };

    var module = angular.module("flightBooker");
    module.factory("dashboardService", ["$resource", dashboardService]);
}());