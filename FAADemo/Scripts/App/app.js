﻿"use strict";

(function () {

    var app = angular.module("flightBooker", ["ngRoute","ngResource","kendo.directives"]);
    app.config([
        "$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
            $routeProvider
                .when("/FlightBooker/Index",
                {
                    templateUrl: "/Templates/home.html",
                    controller: "dashboardController"
                })
                .when("/Flight/Search",
                {
                    templateUrl: "/Templates/flight/search.html",
                    controller: "flightController"
                })
                .when("/Passenger/Add",
                {
                    templateUrl: "/Templates/Passenger/add.html",
                    controller: "passengerController"
                })
                .when("/Airport/Detail",
                {
                    templateUrl: "/Templates/Airport/details.html",
                    controller: "airportController"
                })
                .when("/Airport/Edit",
                {
                    templateUrl: "/Templates/Airport/edit.html",
                    controller: "airportController"
                })
                .when("/Airport/Add",
                {
                    templateUrl: "/Templates/Airport/add.html",
                    controller: "airportController"
                })
                .when("/Airline/Add",
                {
                    templateUrl: "/Templates/Airline/add.html",
                    controller: "airlineController"
                })
                .when("/Airline/Detail",
                {
                    templateUrl: "/Templates/Airline/details.html",
                    controller: "airlineController"
                })
                .otherwise({ redirectTo: "/FlightBooker/Index" });

            $locationProvider.html5Mode(true);
        }
    ]);
}());