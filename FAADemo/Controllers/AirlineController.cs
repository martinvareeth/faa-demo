﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FAADemo.Public.ViewModels;
using FAADemo.Svc.Services;

namespace FAADemo.Controllers
{
    public class AirlineController : ApiController
    {
        private readonly IAirlineService _airlineService;
        public AirlineController(IAirlineService airlineService)
        {
            _airlineService = airlineService;
        }

        public List<AirlineVm> Get()
        {
            return _airlineService.GetAllAirlines();
        }

        public AirlineDetailsVm Get(Guid id)
        {
            return _airlineService.GetAirline(id);
        }

        public HttpResponseMessage Post(HttpRequestMessage request, AirlineVm airline)
        {
            List<string> errors = new List<string>();
            if (ModelState.IsValid)
            {
                _airlineService.AddAirline(airline);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                errors.AddRange(PostErrors());
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, errors);
        }

        private IEnumerable<string> PostErrors()
        {
            return ModelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage));
        }
    }
}
