﻿using AutoMapper;
using FAADemo.Dal.Models;
using FAADemo.Public.ViewModels;

namespace FAADemo.Svc.DataMappers
{
    public class DomainViewModelMapper : Profile
    {
        protected override void Configure()
        {
            ConfigureToDomainModel();
            ConfigureToViewModels();
        }

        private void ConfigureToViewModels()
        {
            Mapper.CreateMap<PassengerVm, Passenger>();
            Mapper.CreateMap<Airport, AirportVm>();
            Mapper.CreateMap<Airline, AirlineVm>();

            Mapper.CreateMap<Flight, SpecialFlightVm>()
                .ForMember(d => d.ArrivalAirport, o => o.MapFrom(s => s.ArrivalAirport.AirportCode))
                .ForMember(d => d.DepartureAirport, o => o.MapFrom(s => s.DepartureAirport.AirportCode))
                .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
                .ForMember(d => d.DepartureDate, o => o.MapFrom(s => s.Departure))
                .ForMember(d => d.Price, o => o.MapFrom(s => s.Price - (s.Price*(s.Discount/100))));

            Mapper.CreateMap<Flight, FlightDetailsVm>()
                .ForMember(d => d.ArrivalLocation, o => o.MapFrom(s => s.ArrivalAirport.AirportCode))
                .ForMember(d=>d.DepartureLocation, o=>o.MapFrom(s=>s.DepartureAirport.AirportCode))
                .ForMember(d=>d.AirplaneType,o=>o.MapFrom(s=>s.AirplaneType.Name))
                .ForMember(d=>d.Price, o=>o.MapFrom(s => s.Price - (s.Price * (s.Discount / 100))));
        }

        private void ConfigureToDomainModel()
        {
            Mapper.CreateMap<Passenger, PassengerVm>();
            Mapper.CreateMap<AirportVm, Airport>();
            Mapper.CreateMap<AirlineVm, Airline>();
        }
    }
}