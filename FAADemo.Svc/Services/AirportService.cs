﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAADemo.Dal;
using FAADemo.Dal.Models;
using FAADemo.Public.ViewModels;

namespace FAADemo.Svc.Services
{
    public class AirportService : IAirportService
    {
        private readonly IFlightBookerRepository _repository;

        public AirportService(IFlightBookerRepository repository)
        {
            _repository = repository;
        }
        public List<AirportVm> GetAllAirports()
        {
            return AutoMapper.Mapper.Map<List<Airport>, List<AirportVm>>(_repository.GetAllAirports());
        }

        public AirportDetailsVm GetAirport(Guid airportId)
        {
            var airport = _repository.GetAirport(airportId);

            return new AirportDetailsVm
            {
                Airport = AutoMapper.Mapper.Map<Airport,AirportVm>(airport),
                ArrivalFlights = AutoMapper.Mapper.Map<List<Flight>, List<FlightDetailsVm>>(airport.ArrivalFlights.ToList()),
                DepartureFlights = AutoMapper.Mapper.Map<List<Flight>, List<FlightDetailsVm>>(airport.DepartureFlights.ToList())
            };
        }

        public void AddAirport(AirportVm airport)
        {
            _repository.AddAirport(AutoMapper.Mapper.Map<AirportVm,Airport>(airport));
        }
    }
}
