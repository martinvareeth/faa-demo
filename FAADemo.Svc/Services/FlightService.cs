﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FAADemo.Dal;
using FAADemo.Dal.Models;
using FAADemo.Public.ViewModels;

namespace FAADemo.Svc.Services
{
    public class FlightService : IFlightService
    {
        private readonly IFlightBookerRepository _repository;

        public FlightService(IFlightBookerRepository repository)
        {
            _repository = repository;
        }

        public List<SpecialFlightVm> GetSpecialFlights()
        {
            return AutoMapper.Mapper.Map<List<Flight>, List<SpecialFlightVm>>(_repository.GetSpecialFlights());
        }

        public List<FlightDetailsVm> GetAllFlights()
        {
            return AutoMapper.Mapper.Map<List<Flight>, List<FlightDetailsVm>>(_repository.GetAllFlights());
        } 

        public FlightDetailsVm GetFlight(Guid flightId)
        {
            return AutoMapper.Mapper.Map<Flight, FlightDetailsVm>(_repository.GetFlight(flightId));
        }
    }
}
